'use strict';
// Define the `promoApp` module
var promoApp = angular.module('promoApp', ['ngRoute', 'angucomplete-alt', 'ngSanitize', 'daterangepicker','ngCookies','ng.deviceDetector']);

// Add authentication to each request
promoApp.config(function ($httpProvider) {
  $httpProvider.interceptors.push('httpRequestInterceptor');
});

promoApp.factory('httpRequestInterceptor', function ($cookies) {
  return {
    request: function (config) {
      var token = $cookies.get('token');
      if (token)
              config.headers["Authorization"] = "Basic " + token;

      return config;
    }
  };
});

promoApp.factory('searchService', function() {
  var savedData = {}
  function set(data) {
    savedData = data;
  }
  function get() {
   return savedData;
  }
 
  return {
   set: set,
   get: get
  }
 });

promoApp.factory('AppSite', function() {
  return {
      site : 'BNA'
  };
});

promoApp.factory('getLogin', function($cookies) {
  
  function username() {
    var myUsername = $cookies.get('username');
    if (myUsername)
        return myUsername;
   
  }
  return {
      username : username
  };
});

promoApp.controller('myip', function($rootScope,$http, $cookies) {
  /*$http.get("https://api.ipify.org/?format=json").then(function (responsmaincon
  e) 
  {
    $rootScope.ip = response.data.ip;
    $cookies.put('ip', response.data.ip);
  });*/ 
  $cookies.put('ip', '127.0.0.1');
});

// configure our routes
promoApp.config(function($routeProvider, $locationProvider) {
  
  $routeProvider
      // route for the home page
      .when('/', {
          templateUrl : '/app/components/home.html',
          controller  : 'mainController',
          sportcat: 'racing',
          sportlabel: 'Horse Racing',
          homeheader: 'Find the Best Available Racing Offer<br/>Everytime You Bet!'
      })
      // route for the contact page
      .when('/logout', {
        templateUrl : '/app/components/home.html',
        controller  : 'logoutController'
      })
      // route for the about page
      .when('/about', {
          templateUrl : '/app/components/about.html',
          controller  : 'aboutController'
      })
      // route for the contact page
      .when('/contact', {
          templateUrl : '/app/components/contact.html',
          controller  : 'contactController'
      })
      // route for the sitemap page
      .when('/sitemap', {
        templateUrl : '/app/shared/app-sitemap.html',
        controller  : 'sitemapController'
      })
      .when('/searchresults', {
        templateUrl : '/app/components/searchresults.html',
        controller  : 'resultController'
      })
      .when('/promo/horse-racing', {
        templateUrl : '/app/components/home.html',
        controller  : 'mainController',
        sportcat: 'racing',
        sportlabel: 'Horse Racing',
        homeheader: 'Find the Best Available Racing Offer<br/>Everytime You Bet!'
      })
      .when('/promo/nrl', {
        templateUrl : '/app/components/home.html',
        controller  : 'mainController',
        sportcat: 'NRL',
        sportlabel: 'NRL',
        homeheader: 'Find the Best Available NRL Offer<br/>Everytime You Bet!'
      })
      .when('/promo/afl', {
        templateUrl : '/app/components/home.html',
        controller  : 'mainController',
        sportcat: 'AFL',
        sportlabel: 'AFL',
        homeheader: 'Find the Best Available AFL Offer<br/>Everytime You Bet!'
      })
      .when('/promo/us-sports', {
        templateUrl : '/app/components/home.html',
        controller  : 'mainController',
        sportcat: 'ussports',
        sportlabel: 'US Sports',
        homeheader: 'Find the Best Available Offer<br/>Everytime You Bet!'
      })
      .when('/promo/other-sports', {
        templateUrl : '/app/components/home.html',
        controller  : 'mainController',
        sportcat: 'Other',
        sportlabel: 'Other Sports',
        homeheader: 'Find the Best Available Offer<br/>Everytime You Bet!'
      })
      .when('/bookieredirect', {
        templateUrl : '/app/components/bookieredirect.html',
        controller  : 'bookredirectController'
      })
      .otherwise({
        redirectTo: '/'
      });
      $locationProvider.html5Mode(true);
      $locationProvider.hashPrefix('');
});

promoApp.controller('NowDate', function ($scope, getLogin, $rootScope) {
    $scope.CurrentDate = new Date();
    $rootScope.username = getLogin.username();
});

promoApp.controller('bookredirectController', function ($scope, $location, deviceDetector) {
  var destination = $location.search().dest;
  $scope.bookmakerRedirect = $location.search().bookmaker;

  // Capture Bookmaker Clicked - record user / session / which bookmaker clicked to join / date time stamp / Device

  var myOS = deviceDetector.raw;

  if( (myOS.os.ios == true || myOS.os.android == true ) && myOS.device.ipad == false ){
        $scope.redirectMessage = '<p>...follow the link in the text we just sent you back to this page to see your offer!</p>';
  }else{
    $scope.redirectMessage = '<p>...click back here to continue & see your offer.</p>';
  }

  // Add a countdown for the redireect to the bookmaker site
  var seconds = 7; // seconds for HTML
  var foo; // variable for clearInterval() function

  function redirect() {
      document.location.href = destination;
  }

  function updateSecs() {
      document.getElementById("seconds").innerHTML = seconds;
      seconds--;
      if (seconds == -1) {
          clearInterval(foo);
          redirect();
      }
  }

  function countdownTimer() {
      foo = setInterval(function () {
          updateSecs()
      }, 1000);
  }
  countdownTimer();
});

promoApp.controller('logoutController', function ($location, $http, $cookies, getLogin, $rootScope) {

      var token = $cookies.get('token');

      $http.get("https://thegreattipoff.com/API/auth.cfc?method=logout&uid=" + token)
      .then(function successCallback(response){
          console.log("Logged Out");
      }, function errorCallback(response){
          console.log("Unable to perform get request");
      });

      $cookies.remove('token');
      $cookies.remove('username');
      $rootScope.username = getLogin.username();
      $location.path('/');
});

// create the controller and inject Angular's $scope
promoApp.controller('mainController', function($scope, $route, $http, $rootScope,deviceDetector) {
    $scope.isMobile = false;
    if(!deviceDetector.isDesktop()){
     $scope.isMobile = true;
    }
    $scope.sportcat = $route.current.$$route.sportcat;
    $scope.sportlabel = $route.current.$$route.sportlabel;
    $scope.homeHeader = $route.current.$$route.homeheader;

    

    $scope.updateResults = function(message) {
        if(angular.isObject(message) == true){
          $scope.searchresponse = message;
          $scope.searcherrormessage = message.data.STATUSMESSAGE;
          $scope.mypromotions = message.data.MYPROMOS;
          $scope.totalpromotions = message.data.TOTALPROMOS;
          $scope.rhspromoadlink = message.data.ADSET.RHSLINK;
          $scope.rhspromoadad= "assets/images/advert/" + message.data.ADSET.RHSADIMAGE;
        }else{
          $scope.searchresponse = {};
          $scope.searcherrormessage = message;
          $scope.mypromotions = 0;
          $scope.totalpromotions = 0;
          $scope.rhspromoadlink = "/searchresults";
          $scope.rhspromoadad= "assets/images/advert/defaultad300px.png";
        }
    };

    

    $rootScope.$on('customEvent', function(event, message) {
      $scope.updateResults(message);
    });

    $scope.searchplace = 'Type here e.g. NRL or Tigers or Rugby League or First Try Scorer';
    $http.get("https://thegreattipoff.com/API/promotool.cfc?method=getPromosummary&sportscat=" + $scope.sportcat)
        .then(function successCallback(response){
              if(response.data.length < 1){
                $scope.errormessage ="Currently no promotions available for " + $scope.sportlabel ;
              }else{
                $scope.response = response;
              }
        }, function errorCallback(response){
            console.log("Unable to perform get request");
        });

        $scope.remoteUrlRequestFn = function(str) {
          return {q: str};
        };
    
        $scope.inputChanged = function(str) {
          $scope.console10 = str;
        }
        $scope.focusIn = function() {
          var focusInputElem = document.getElementById('ex12_value');
          $scope.focusState = 'In';
          focusInputElem.classList.remove('small-input');
        }
        $scope.focusOut = function() {
          var focusInputElem = document.getElementById('ex12_value');
          $scope.focusState = 'Out';
          focusInputElem.classList.add('small-input');
        }
    
        /***
         * Send a broadcast to the directive in order to clear itself
         * if an id parameter is given only this ancucomplete is cleared
         * @param id
         */
        $scope.clearInput = function (id) {
          if (id) {
            $scope.$broadcast('angucomplete-alt:clearInput', id);
          }
          else{
            $scope.$broadcast('angucomplete-alt:clearInput');
          }
        }
    
        $scope.disableInput = true;
    
        $scope.requireExample8a = true;
        $scope.requireExample8b = true;
});

promoApp.controller('resultChartCtrl', function($scope) {

    $scope.rankbar = ($scope.x.RANK*100)/36;
    //setting className of progressbar
    $scope.progressValueClass = $scope.x.VALUE < 50 ? "bg-danger" :
          ($scope.x.VALUE > 50 &&  $scope.x.VALUE < 75) ? "bg-warning" : "bg-success";

    $scope.progressRankClass = $scope.rankbar < 50 ? "bg-danger" :
      ($scope.rankbar > 50 &&  $scope.rankbar < 75) ? "bg-warning" : "bg-success";
      
      $scope.toggleDetails = function () {
        if(!$scope.showDetails){
        $scope.showDetails = true;
        }else{
          $scope.showDetails = false;
        }
       };
});

promoApp.controller('bookieController', function($scope, $cookies, $http) {
  
  $scope.clearInput = function (id) {
    if (id) {
      $scope.$broadcast('angucomplete-alt:clearInput', id);
    }
    else{
      $scope.$broadcast('angucomplete-alt:clearInput');
    }
  }

  $scope.getmyBookies = function () {
    var token = $cookies.get('token');

    if(token === undefined){
        token = '2F6A9000B8178CA3BB268C87F01ED82D';
    }

    $http.get("https://thegreattipoff.com/API/promotool.cfc?method=getmyBookies&token=" + token)
        .then(function successCallback(response){
              $scope.bookieresponse = response;
        }, function errorCallback(response){
            console.log("Unable to perform get request");
        });
    }

    $scope.getpendingBookies = function () {
      var token = $cookies.get('token');
  
      if(token === undefined){
          token = '2F6A9000B8178CA3BB268C87F01ED82D';
      }
  
      $http.get("https://thegreattipoff.com/API/promotool.cfc?method=getmyBookies&pending=true&token=" + token)
          .then(function successCallback(response){
                if(Object.keys(response.data).length > 0){
                      //trigger popup with list of bookies to approve
                      $scope.bookiePendingresponse = response;
                } else {
                      $('#bookieModal').modal('hide');
                }
                
          }, function errorCallback(response){
              console.log("Unable to perform get request");
          });
      }
  
  $scope.addBookie = function (mybookie) {
      var token = $cookies.get('token');
  
      if(token === undefined){
          token = '2F6A9000B8178CA3BB268C87F01ED82D';
      }
      $http.get("https://thegreattipoff.com/API/promotool.cfc?method=addBookie&bookieid=" + mybookie + "&token=" + token)
          .then(function successCallback(response){
              $scope.getmyBookies();
              $scope.$broadcast('angucomplete-alt:clearInput', 'searchbookie-1');
              $scope.$broadcast('angucomplete-alt:clearInput', 'searchbookie-2');
          }, function errorCallback(response){
              console.log("Unable to perform get request");
          });
  }
  
  $scope.removeBookie = function (mybookie) {
    var token = $cookies.get('token');

      if(token === undefined){
          token = '2F6A9000B8178CA3BB268C87F01ED82D';
      }
      $http.get("https://thegreattipoff.com/API/promotool.cfc?method=removeBookie&bookieid=" + mybookie + "&token=" + token)
          .then(function successCallback(response){
            $scope.getmyBookies();
            $scope.getpendingBookies();
            $scope.$broadcast('angucomplete-alt:clearInput', 'searchbookie');
          }, function errorCallback(response){
              console.log("Unable to perform get request");
          });
    }

    $scope.addpendingBookie = function (mybookie) {
      var token = $cookies.get('token');
  
      if(token === undefined){
          token = '2F6A9000B8178CA3BB268C87F01ED82D';
      }
  
      $http.get("https://thegreattipoff.com/API/promotool.cfc?method=addBookie&bookieid=" + mybookie + "&token=" + token + '&pending=true')
          .then(function successCallback(response){
              $scope.getmyBookies();
              $scope.$broadcast('angucomplete-alt:clearInput', 'searchbookie-1');
              $scope.$broadcast('angucomplete-alt:clearInput', 'searchbookie-2');
          }, function errorCallback(response){
              console.log("Unable to perform get request");
          });
    }

    $scope.authBookie = function (mybookie) {
      var token = $cookies.get('token');
  
      if(token === undefined){
          token = '2F6A9000B8178CA3BB268C87F01ED82D';
      }
  
      $http.get("https://thegreattipoff.com/API/promotool.cfc?method=authoriseBookie&bookieid=" + mybookie + "&token=" + token)
          .then(function successCallback(response){
              $scope.getmyBookies();
              $scope.getpendingBookies();
              $scope.$broadcast('angucomplete-alt:clearInput', 'searchbookie-1');
              $scope.$broadcast('angucomplete-alt:clearInput', 'searchbookie-2');
          }, function errorCallback(response){
              console.log("Unable to perform get request");
          });
    }
});

promoApp.controller('loginController', function($scope, $http, AppSite, $cookies, $rootScope, getLogin) {
  $scope.loginerror = undefined;
  $scope.ngLoginError = false;

  $scope.getLogin = function(){  

    $http.get("https://thegreattipoff.com/API/auth.cfc?method=validate&uid=" + $scope.loginUsername + "&password=" + $scope.loginPassword + "&siteview=" + AppSite.site)
    .then(function successCallback(response){
          if(response.data.LOGINSTATUS == false ){
              $scope.loginerror = "Login Failed. Check your details"; 
              $scope.ngLoginError = true;
          }else{
            // check or set the cookie
            $scope.loginerror = undefined;
            $scope.loginPassword='';
            $scope.loginUsername='';
            $('#loginModal').modal('hide');
            
            $cookies.put('token', response.data.TOKEN, {'expires': response.data.TOKENEXPIRE});
            $cookies.put('username',response.data.FIRSTNAME, {'expires': response.data.TOKENEXPIRE});
            
            $rootScope.username = getLogin.username();
            // replacecontainer with the is logged in details
            //$scope.events = response.data;  
          }
    }, function errorCallback(response){
        console.log("Unable to perform get request");
    });
  }  
});

promoApp.controller('joinController', function($scope, $http, AppSite, $cookies, $rootScope, getLogin) {
  $scope.joinerror = undefined;
  $scope.joinsuccess = undefined;
  $scope.errorFirstname = undefined;
  $scope.errorMobile= undefined;
  $scope.errorPassword= undefined;
  $scope.errorConfpassword = undefined;
  $scope.errorState = undefined;
  $scope.errorTerms = undefined;

  $scope.toolname = "Promo Power Tool";

  $scope.sendJoin = function(){  
    var formdata=$scope.fields;

    $scope.joinerror = undefined;
    $scope.joinsuccess = undefined;
    $scope.errorFirstname = undefined;
    $scope.errorMobile= undefined;
    $scope.errorPassword= undefined;
    $scope.errorConfpassword = undefined;
    $scope.errorState = undefined;
    $scope.errorTerms = undefined;

    $scope.objectip = {    
      ip: '127.0.0.1'
    };
    angular.extend(formdata, $scope.objectip);

    $http.get("https://thegreattipoff.com/API/auth.cfc?method=joinPromo&formval=" + JSON.stringify(formdata) + "&siteview=" + AppSite.site)
    .then(function successCallback(response){
          if(response.data.ERRORCHECK.ERRORS == 0 ){
              // Join success
              // Put success message on screen

              // auto login user into site, update cookies
              $cookies.put('token', response.data.TOKEN, {'expires': response.data.TOKENEXPIRE});
              $cookies.put('username',response.data.FIRSTNAME, {'expires': response.data.TOKENEXPIRE});
            
              $rootScope.username = getLogin.username();
              $scope.joinsuccess = "A verification ID was just sent as a text to your mobile phone. Please enter the ID below, submit & you’re ready to go!";

              $scope.jointabclass = "disabled";
              $scope.bookietabclass = "active";

              angular.element('tab a[data-target="#nav-bookie-tab]').tab('show');
          
          }else{
            // Join fail - return error message
            $scope.joinerror = "Sign up Failed. Please check your details."; 
            if(response.data.ERRORCHECK.JOINFIRSTNAME.length > 1){
                $scope.errorFirstname = response.data.ERRORCHECK.JOINFIRSTNAME;
            }
            if(response.data.ERRORCHECK.JOINMOBILE.length > 1){             
              $scope.errorMobile= response.data.ERRORCHECK.JOINMOBILE;
            }
            if(response.data.ERRORCHECK.JOINPASSWORD.length > 1){
                $scope.errorPassword= response.data.ERRORCHECK.JOINPASSWORD;
            }
            if(response.data.ERRORCHECK.JOINCONFPASSWORD.length > 1){
              $scope.errorConfpassword = response.data.ERRORCHECK.JOINCONFPASSWORD;
            }
            if(response.data.ERRORCHECK.JOINSTATE.length > 1){
                $scope.errorState = response.data.ERRORCHECK.JOINSTATE;
            }
            if(response.data.ERRORCHECK.JOINTERMS.length > 1){
                $scope.errorTerms = response.data.ERRORCHECK.JOINTERMS;
            }
            // replacecontainer with the is logged in details
            //$scope.events = response.data;  
          }
    }, function errorCallback(response){
        console.log("Unable to perform get request");
    });
  }  
});

promoApp.controller('aboutController', function($scope) {
  $scope.message = 'Look! I am an about page.';
});
promoApp.controller('contactController', function($scope) {
  $scope.message = 'Contact us! JK. This is just a demo.';
});
promoApp.controller(  'sitemapController', function($scope) {
  $scope.message = 'Here in the Sitemap';
});
promoApp.controller('resultController', function($scope, searchService, $http, $cookies) {
  $scope.results = searchService.get();

  $scope.maxbet = 1500;
  $scope.minbet = 5;
  $scope.maxbetvalue = 1500;
  $scope.maxbetdisplay = 1500;
  // Default Ad link
  $scope.rhspromoadlink = "/searchresults";
  $scope.rhspromoadad= "assets/images/advert/defaultad300px.png";
  
  // Check whether the user logged in and then if any user has a bookie they have not verified
  var token = $cookies.get('token');

  if(token === undefined){
        token = '2F6A9000B8178CA3BB268C87F01ED82D';
  }
  $http.get("https://thegreattipoff.com/API/promotool.cfc?method=getmyBookies&pending=true&token=" + token)
      .then(function successCallback(response){
            // if response len, then show popup
            //trigger popup with list of bookies to approve
            if(Object.keys(response.data).length > 0){
                  //trigger popup with list of bookies to approve
                  $('#bookieModal').modal('show');
                  $scope.bookiePendingresponse = response;
            } else {
                  $('#bookieModal').modal('hide');
                  
                  $http.get("https://thegreattipoff.com/API/promotool.cfc?method=getmyBookies&token=" + token)
                      .then(function successCallback(responseany){
                            // if response len, then show popup
                            //trigger popup with list of bookies to approve
                            if(Object.keys(responseany.data).length <= 0){
                                  //trigger popup with list of bookies to approve
                                  $('#nobookieModal').modal('show');
                            }
                      }, function errorCallback(response){
                          console.log("Unable to perform get request");
                  });
            }
      }, function errorCallback(response){
          console.log("Unable to perform get request");
  });
  
  // Initialise search bar
  $scope.initvalue = {
    SEARCHVAL: $scope.results.SEARCHVAL
  }
  // Date Picker Initialise
  $scope.selectedDaterange = {
    startDate: moment().subtract(1, "days"),
    endDate: moment()
  };

  $scope.setStartDate = function () {
    $scope.selectedDaterange.startDate = moment().subtract(4, "days").toDate();
  };

  $scope.setRange = function () {
      $scope.selectedDaterange = {
          startDate: moment().subtract(5, "days"),
          endDate: moment()
      };
  };
  // END Date picker
  $scope.eventlabel = "Events";
  $scope.matchlabel = "Matches";

  //Set the range for the maxbet search bar
  function updateMaxbet (maxbet, refresh, reset) {
      if(angular.element("#maxBetRange").length) {
            if(maxbet === undefined) {

            } else {
                $scope.maxbet = maxbet;
                
                if(maxbet < 10){
                  $scope.stepval = 1;
                  $scope.minbet = 1;
                }else if(maxbet < 150){
                    $scope.stepval = 5;
                    $scope.minbet = 5;
                }else{
                  $scope.stepval = 50;
                  $scope.minbet = 50;
                }
                //$scope.maxbetvalue = maxbet;
                if(reset == true){
                    $scope.searchFilter.maxBetRange = maxbet;
                    if(refresh == true){
                        $scope.maxbetdisplay = maxbet;
                    }
                }
          }
        }
  }

// getEvents
  $http.get("https://thegreattipoff.com/API/promotool.cfc?method=getPromo&token=" + token +"&q=" + JSON.stringify($scope.results))
    .then(function successCallback(response){
          if(response.data.length < 1){ 
            $scope.$emit('customEvent', response.data.statusmessage);
          }else{
            $scope.$emit('customEvent', response);
            updateMaxbet(response.data.MAXBETVALUE, true, true);
          }
    }, function errorCallback(response){
        console.log("Unable to perform get request");
    });

  $scope.loadEvents = function(){  
    
    $http.get("https://thegreattipoff.com/API/promotool.cfc?method=getEvents&sport=" + $scope.searchFilter.selectedSport + "&daterange=" + JSON.stringify($scope.selectedDaterange))
    .then(function successCallback(response){
          if($scope.searchFilter.selectedSport == 11 || $scope.searchFilter.selectedSport == 12 || $scope.searchFilter.selectedSport ==13){
            $scope.eventlabel = "Locations";
            $scope.matchlabel = "Races";
          }else{
            $scope.eventlabel = "Events";
            $scope.matchlabel = "Matches";
          }
          $scope.events = response.data; 
          updateMaxbet(1500, false, true);
          $scope.loadMatches();

          var formData = JSON.stringify($scope.searchFilter);

          $http.get("https://thegreattipoff.com/API/promotool.cfc?method=getPromo&token=" + token + "&q={}&formData=" + JSON.stringify(formData))
              .then(function successCallback(responseUpdate){
                    if(responseUpdate.data.length < 1){ 
                      $scope.$emit('customEvent', responseUpdate.data.statusmessage);
                      
                      //$scope.searcherrormessage ="No promotions available from your bookmakers for your selected criteria.";
                    }else{
                      $scope.$emit('customEvent', responseUpdate);
                      updateMaxbet(responseUpdate.data.MAXBETVALUE, true, true);
                      //$scope.searchresponse = response;
                    }
              }, function errorCallback(responseUpdate){
                  console.log("Unable to perform get request");
              });

    }, function errorCallback(response){
        console.log("Unable to perform get request");
    }); 
  }  
  
  $scope.loadMatches = function(){  
  
   if(  $scope.searchFilter.selectedEvent === undefined || $scope.searchFilter.selectedEvent === null){
       var myevent = ''; 
   }else{
       var myevent = $scope.searchFilter.selectedEvent; 
   } 

  $http.get("https://thegreattipoff.com/API/promotool.cfc?method=getMatches&sport=" + $scope.searchFilter.selectedSport + "&event=" + myevent)
    .then(function successCallback(response){
          $scope.matches = response.data;

          var formData = JSON.stringify($scope.searchFilter);

          $http.get("https://thegreattipoff.com/API/promotool.cfc?method=getPromo&token=" + token + "&q=" + JSON.stringify($scope.results) + "&formData=" + JSON.stringify(formData))
              .then(function successCallback(responseUpdate){
                    if(responseUpdate.data.length < 1){ 
                      $scope.$emit('customEvent', responseUpdate.data.statusmessage);
                      //$scope.searcherrormessage ="No promotions available from your bookmakers for your selected criteria.";
                    }else{
                      $scope.$emit('customEvent', responseUpdate);
                      updateMaxbet(responseUpdate.data.MAXBETVALUE, true, true);
                      //$scope.searchresponse = response;
                    }
              }, function errorCallback(responseUpdate){
                  console.log("Unable to perform get request");
              });
    }, function errorCallback(response){
        console.log("Unable to perform get request");
    }); 
  } 

  $scope.getMatchPromo = function(){  

      var formData = JSON.stringify($scope.searchFilter);
      $http.get("https://thegreattipoff.com/API/promotool.cfc?method=getPromo&token=" + token + "&q=" + JSON.stringify($scope.results) + "&formData=" + JSON.stringify(formData))
          .then(function successCallback(responseUpdate){
                if(responseUpdate.data.length < 1){ 
                  $scope.$emit('customEvent', responseUpdate.data.statusmessage);
                  //$scope.searcherrormessage ="No promotions available from your bookmakers for your selected criteria.";
                }else{
                  $scope.$emit('customEvent', responseUpdate);
                  updateMaxbet(responseUpdate.data.MAXBETVALUE, true, true);
                  //$scope.searchresponse = response;
                }
          }, function errorCallback(responseUpdate){
              console.log("Unable to perform get request");
          });
  }

  $scope.getPromo= function(){  

    var formData = JSON.stringify($scope.searchFilter);
    $http.get("https://thegreattipoff.com/API/promotool.cfc?method=getPromo&token=" + token + "&q=" + JSON.stringify($scope.results) + "&formData=" + JSON.stringify(formData))
        .then(function successCallback(responseUpdate){
              if(responseUpdate.data.length < 1){ 
                $scope.$emit('customEvent', responseUpdate.data.statusmessage);
                //$scope.searcherrormessage ="No promotions available from your bookmakers for your selected criteria.";
              }else{
                $scope.$emit('customEvent', responseUpdate);
                updateMaxbet(responseUpdate.data.MAXBETVALUE, true, false);
                //$scope.searchresponse = response;
              }
        }, function errorCallback(responseUpdate){
            console.log("Unable to perform get request");
        });
}

  $scope.loadSports = function(){  
    $http.get("https://thegreattipoff.com/API/promotool.cfc?method=getSports")
    .then(function successCallback(response){
          $scope.sports = response.data; 
    }, function errorCallback(response){
          console.log("Unable to perform get sports request");
    }); 
  } 

  $scope.loadPromotypes = function(){  
    $http.get("https://thegreattipoff.com/API/promotool.cfc?method=getPromoFieldType&fieldtype=promotype")
    .then(function successCallback(response){
          $scope.promotype = response.data; 
    }, function errorCallback(response){
          console.log("Unable to perform get sports request");
    }); 
  } 

  $scope.loadBenefittype = function(){  
    $http.get("https://thegreattipoff.com/API/promotool.cfc?method=getPromoFieldType&fieldtype=BenefitType")
    .then(function successCallback(response){
          $scope.benefittype = response.data; 
    }, function errorCallback(response){
          console.log("Unable to perform get sports request");
    }); 
  } 

  $scope.loadMybookies = function(){  
    var token = $cookies.get('token');

    if(token === undefined){
        token = '2F6A9000B8178CA3BB268C87F01ED82D';
    }

    $http.get("https://thegreattipoff.com/API/promotool.cfc?method=getmyBookies&token=" + token)
        .then(function successCallback(response){
              $scope.bookiecount = response.data.length;
              $scope.bookieList = response.data;
        }, function errorCallback(response){
            console.log("Unable to perform get request");
        });
  }
  
  $scope.remoteUrlRequestFn = function(str) {
    return {q: str};
  };

  $scope.inputChanged = function(str) {
    $scope.console10 = str;
  }
  $scope.focusIn = function() {
    var focusInputElem = document.getElementById('ex12_value');
    $scope.focusState = 'In';
    focusInputElem.classList.remove('small-input');
  }
  $scope.focusOut = function() {
    var focusInputElem = document.getElementById('ex12_value');
    $scope.focusState = 'Out';
    focusInputElem.classList.add('small-input');
  }

  /***
   * Send a broadcast to the directive in order to clear itself
   * if an id parameter is given only this ancucomplete is cleared
   * @param id
   */
  $scope.clearInput = function (id) {
    if (id) {
      $scope.$broadcast('angucomplete-alt:clearInput', id);
    }
    else{
      $scope.$broadcast('angucomplete-alt:clearInput');
    }
}
  $scope.disableInput = true;

  $scope.toggleFilter = function () {
    if(!$scope.showFilter){
    $scope.showFilter = true;
    }else{
      $scope.showFilter = false;
    }
   };
   
});

promoApp.controller('searchController', function($scope, $location, $route, searchService) {
      // process the form
      $scope.processSearch = function(searchvalue) {
          var searchvalue = $scope.selectedProject.originalObject;

          searchService.set(searchvalue);

          if(location.pathname === '/searchresults') {
            $route.reload();
          } else {
            $location.path('/searchresults');
          }
          
      };
});

promoApp.controller('showhidectrl', function ($scope) {
  $scope.ngShowhide = true;
  
  $scope.ngShowhideFun = function(flag) {
        if (flag) {
            $scope.ngShowhide = false;
        } else {
            $scope.ngShowhide = true;
        }
  };
});

